<?php

require('./models/db.php');
require('./models/user.php');
include('./views/login.php');
$db=connectdb();

if(is_connected()) {
    $user= getUser($db, $_SESSION['id']);

    if($user['rank'] == 'ORGANIZER') {require('');}//redirect to organizer calendar
    if($user['rank'] == 'CUSTOMER') {require('');}// redirect to customer calendar
}
else {
    require('./controllers/login.php');
}