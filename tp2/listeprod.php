<?php
$handle = fopen('stock.csv');
while (($produit = fgetcsv($handle, 0, ";")) !== FALSE) { ?>
  <div style="border: solid black; text-align: center; padding: 15px;">
    <p>Nom du produit : <?= $produit[0] ?></p>
    <p>Prix du produit : <?= $produit[1] ?></p>
    <p>Image du produit : <img src="<?= $produit[3] ?>" width="200px"></p>
  </div>
  <br>
<?php }
fclose($handle);
