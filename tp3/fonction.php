<?php
function startBdd(){
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
    }
    catch (Exception $e) {
        exit('Erreur de connexion à la base de données.');
    }
}

function addInBdd($date,$name,$message){
    $wait = $bdd->prepare('INSERT INTO messages(name, message, date) VALUES(:name, :message, :date)');
    $wait->execute([":name"=>$name, ":message"=>$message, ":post_date"=>[$date]]);
}